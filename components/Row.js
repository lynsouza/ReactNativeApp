import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    flexDirection: 'row',
    //alignItems: 'center',
    height: 100,
  },
  description: {
    flexDirection: 'row'
  },
  descriptionCollun: {
    paddingTop: 20
  },
  ratingView: {
    height: 30,
    width: 30,
    alignItems: 'center',
    paddingTop: 7,
    borderRadius: 5
  },
  localName: {
    marginLeft: 12,
    fontSize: 22,
    color: "#D06600",
  },
  localDescription: {
    marginLeft: 12,
    fontSize: 16,
    color: "#717171",
    paddingRight: 10,
  },
  photo: {
    height: 90,
    width: 120,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.5
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
    marginTop: 8
  },
});

const Row = (props) => (
  <View style={{ justifyContent: "space-between"}}>
    <View style={styles.container}>
      <Image source={ props.picture }  style={[styles.photo, {flex:1}]} />
      <View style={{flex:2}} >

        <View style={{flexDirection: 'row', justifyContent: "space-between"}}>
          <Text style={styles.localName}>
            {`${props.name}`}
          </Text>

          <View style={[styles.ratingView, {backgroundColor: (props.rating>9.0) ? "#3CCD36" : (props.rating>8.0) ? "#93CD36" : (props.rating>7.0) ? "#CBCD36" : "#CD6136" }]}>
            <Text style={{color: "#FFFFFF"}}>
              {`${props.rating}`}
            </Text>
          </View>
        </View>

        <View style={[styles.description, {flex:10}]}>

          <View tyle={[styles.descriptionCollun, {flex:1, paddingTop:10}]}>
            <Text style={styles.localDescription}>
              {`${props.type}`}
            </Text>

            <Text style={[styles.localDescription, {paddingTop:10}]}>
              {`${props.distance}`}
            </Text>
          </View>

          <View tyle={[styles.descriptionCollun, {flex:1, paddingTop:10}]}>
            <Text style={styles.localDescription}>
              {`${props.expensive}`}
            </Text>

            <Text style={[styles.localDescription, {paddingTop:10}]}>
              {`${props.city}`}
            </Text>
          </View>

        </View>

      </View>
    </View>
    <Text style={styles.localDescription}>
      "{`${props.coment}`}"
    </Text>
    <View style={styles.separator} />
  </View>

);

// function getColor(valor) {
//   if valor>9.0 {
//     return(backgroundColor: "#3CCD36")
//   } else if valor>8.0 {
//     return(backgroundColor: "#93CD36")
//   } else if valor > 7.0 {
//     return(backgroundColor: "#CBCD36")
//   }
//   return(backgroundColor: "#CD6136")
// }

export default Row;

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  ListView,
  FlatList,
  Text,
  View
} from 'react-native';
import { Card } from "react-native-elements";

import Row from './components/Row'
import Header from './components/Header'
import SectionHeader from './components/SectionHeader'
import Footer from './components/Footer'
import data from './data'
import Icon from 'react-native-vector-icons/FontAwesome'

const categoryData = [
  {
    image: require('./img/pizza_square.png'),
    title: "Pizza"
  },
  {
    image: require('./img/cafe_square.png'),
    title: "Cafés"
  },
  {
    image: require('./img/japanese_square.png'),
    title: "Japonesa"
  },
  {
    image: require('./img/burger_square.png'),
    title: "Hamburger"
  },
  {
    image: require('./img/vegetarian_square.png'),
    title: "Vegetariana"
  },
  {
    image: require('./img/pizza_square.png'),
    title: "6"
  }
];

export default class App extends React.Component {
  constructor(props) {
    super(props)

    const getSectionData = (dataBlob, sectionId) => dataBlob[sectionId];
    const getRowData = (dataBlob, sectionId, rowId) => dataBlob[`${rowId}`];

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged : (s1, s2) => s1 !== s2,
      getSectionData,
      getRowData,
    });

    const { dataBlob, sectionIds, rowIds } = this.formatData(data);

    this.state = {
      categoryData: categoryData,
      dataSource: ds.cloneWithRowsAndSections(dataBlob, sectionIds, rowIds)
    }
  }

  formatData(data) {
    // We're sorting by alphabetically so we need the alphabet
    const alphabet = [9.5,8.5,7.5,6.5];

    // Need somewhere to store our data
    const dataBlob = {};
    const sectionIds = [];
    const rowIds = [];

    // Each section is going to represent a letter in the alphabet so we loop over the alphabet
    for (let sectionId = 0; sectionId < alphabet.length; sectionId++) {
      // Get the character we're currently looking for
      const currentChar = alphabet[sectionId];

      // Get users whose first name starts with the current letter
      const users = data.filter((user) => user.rating === currentChar);

      // If there are any users who have a first name starting with the current letter then we'll
      // add a new section otherwise we just skip over it
      if (users.length > 0) {
        // Add a section id to our array so the listview knows that we've got a new section
        sectionIds.push(sectionId);

        // Store any data we would want to display in the section header. In our case we want to show
        // the current character
        dataBlob[sectionId] = { character: currentChar };

        // Setup a new array that we can store the row ids for this section
        rowIds.push([]);

        // Loop over the valid users for this section
        for (let i = 0; i < users.length; i++) {
          // Create a unique row id for the data blob that the listview can use for reference
          const rowId = `${sectionId}:${i}`;

          // Push the row id to the row ids array. This is what listview will reference to pull
          // data from our data blob
          rowIds[rowIds.length - 1].push(rowId);

          // Store the data we care about for this row
          dataBlob[rowId] = users[i];
        }
      }
    }

    return { dataBlob, sectionIds, rowIds };
  }


  render() {
    return (
      <View style={styles.container}>
      <View style={{flexDirection: 'row',height: 70, backgroundColor: "#D06600", alignItems: 'center'}}>
        <Icon style={styles.icon} name="search" flex={1} size={30} color="#FFF" />
        <Text style={{color:"#FFFFFF", fontSize: 30, textAlign:'center', flex:3}}>
        Restaurantes
        </Text>
        <Icon style={styles.icon} name="share-alt" flex={1} size={30} color="#FFF" />
      </View>

      <FlatList
        horizontal
        style={{height: 200}}
        data={this.state.categoryData}
        renderItem={({ item: rowData }) => {
          return (
            <Card
              title={null}
              image={rowData.image}
              containerStyle={{ padding: 0, width: 140, height: 180 }}
            >
              <Text style={{ fontSize:18 }}>
                {rowData.title}
              </Text>
            </Card>
          );
        }}
        keyExtractor={(item, index) => index}
      />

      <ListView
      style={styles.list1}
      dataSource={this.state.dataSource}
      renderRow={(data) => <Row {...data} />}
      renderSeparator={ () => <View style={styles.separator} />}
      renderHeader={() => <Header />}
      renderFooter={() => <Footer />}
      renderSectionHeader={(sectionData) => <SectionHeader {...sectionData} />}
      />

      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    marginTop: 20,
  },
  list1: {
    //flex:1
    height: 380
  },
  icon: {
    padding:10
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});

AppRegistry.registerComponent('ListViewDemo', () => ListViewDemo);
